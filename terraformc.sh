#/bin/bash

# expand alias
shopt -s expand_aliases

# terraform commands
OPTIONS=$1

docker run -it --rm --env-file private/env.list -v $PWD:/usr/terransible jrsalgado/terraform:latest $OPTIONS
