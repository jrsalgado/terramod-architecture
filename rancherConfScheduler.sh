#!/bin/bash
# Try 2 times to configure with a delay of 1 minute
COUNT=0
MAXRETRY=20
RETRYDELAY=60
ACTION=$1

# functions definition

# apply
apply(){
docker run -it --rm -v $PWD:/usr/terransible --env-file private/env.list  jrsalgado/terraform:latest apply -auto-approve -var-file=envs/test/terraform.tfvars -target=module.rancher -target=module.rancher_hosts envs/test
}

# plan
plan(){
docker run -it --rm -v $PWD:/usr/terransible --env-file private/env.list  jrsalgado/terraform:latest plan -var-file=envs/test/terraform.tfvars -target=module.rancher -target=module.rancher_hosts envs/test
}

destroy(){
# destroy
docker run -it --rm -v $PWD:/usr/terransible --env-file private/env.list  jrsalgado/terraform:latest destroy -var-file=envs/test/terraform.tfvars -target=module.rancher -target=module.rancher_hosts envs/test
}



while [ $COUNT -lt $MAXRETRY ]; do
case "$ACTION" in
  "plan") plan
  ;;
  "apply") apply
  ;;
  "destroy") destroy
  ;;
  *) echo "usage args [ plan ] [ apply ] [ destroy ]"
     exit 2
  ;;
esac
if [ $? -gt 0 ]; then
    echo "Retry script in $RETRYDELAY seconds"
    COUNT=$[$COUNT+1]
    if [ $COUNT -eq $MAXRETRY ]; then
        exit 2
    fi
    sleep $RETRYDELAY
else
    exit 0
fi

done
exit 0
